package com.nam.quy.design_shop.ui.product;


import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class ProductPresenter implements ProductContract.Presenter {
    private final String TAG = "ProductPresenter";
    private final ProductContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    ProductPresenter(ProductContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }
    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        compositeDisposable.dispose();
    }

    @Override
    public void addOrder(String productId, String number) {

    }
}
