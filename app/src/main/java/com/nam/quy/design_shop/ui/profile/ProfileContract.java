package com.nam.quy.design_shop.ui.profile;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;
import com.nam.quy.design_shop.data.source.remote.response.Order;

import java.util.List;

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void logout();
        void listOrder(List<Order> orderList);
    }
    interface Presenter extends BasePresenter {
        void logout();
        void listOrder();
}
}
