package com.nam.quy.design_shop.ui.product;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = ProductPresenterModule.class)
public interface ProductComponent {
    void inject(ProductFragment fragment);
}