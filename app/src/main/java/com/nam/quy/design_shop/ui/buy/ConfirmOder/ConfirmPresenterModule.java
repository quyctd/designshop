package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import dagger.Module;
import dagger.Provides;

@Module
public class ConfirmPresenterModule {
    private final ConfirmContract.View view;

    public ConfirmPresenterModule(ConfirmContract.View view) {
        this.view = view;
    }

    @Provides
    ConfirmContract.View provideTasksContractView() {
        return view;
    }
}
