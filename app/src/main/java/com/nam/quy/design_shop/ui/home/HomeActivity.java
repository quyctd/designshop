package com.nam.quy.design_shop.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {
    public static Intent intent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        ButterKnife.bind(this);
        HomeFragment homeFragment =
                (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), homeFragment, R.id.container);
        }
    }
}
