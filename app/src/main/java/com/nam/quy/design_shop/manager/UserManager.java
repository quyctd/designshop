package com.nam.quy.design_shop.manager;

import android.content.Context;

import com.nam.quy.design_shop.data.source.remote.response.User;

public class UserManager {
    private final Context context;

    public UserManager(Context context) {
        this.context = context;
    }


    public void saveUser(User user) {
        Pref.saveAuth(context, user);
    }

    public User getUser() {
        User user = Pref.getAuth(context);
        if (user == null) {
            user = new User();
        }
        return user;
    }

//    public boolean isLogged() {
//        return Pref.getToken(context) != null;
//    }

    public void logout() {
        Pref.reset(context);
    }
}
