package com.nam.quy.design_shop.ui.buy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.buy.ConfirmOder.ConfirmActivity;
import com.nam.quy.design_shop.ui.order.OrderActivity;
import com.nam.quy.design_shop.util.Util;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BuyFragment extends BaseFragment implements BuyContract.View {
    @Inject
    BuyPresenter presenter;

    Unbinder unbinder;

    @OnClick(R.id.img_back)
    void back() {
        getActivity().onBackPressed();
    }

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_phone)
    EditText edtPhone;
    @BindView(R.id.edt_address)
    EditText edtAddress;

    @OnClick(R.id.btn_order)
    void order() {


        presenter.oder(edtName.getText().toString(), edtPhone.getText().toString(), edtAddress.getText().toString());
    }

    String name, phone, address;

    public void setup(String name, String phone, String address) {
        if (name == null) name = "";
        if (phone == null) phone = "";
        if (address == null) address = "";
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public void todo() {

        edtName.setText(name);
        edtAddress.setText(address);
        edtPhone.setText(phone);
    }

    public static BuyFragment newInstance() {
        BuyFragment fragment = new BuyFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerBuyComponent.builder()
                .buyPresenterModule(new BuyPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buy_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        todo();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void oderFail(String text) {
        Util.showMessenger("Không thành công, vui lòng thử lại!", getContext());
    }

    @Override
    public void oderSuccess(String text) {
        Util.showMessenger("Cập nhật thông tin mua hàng thành công!", getContext(), new Util.OnCallBack() {
            @Override
            public void todo() {
                getActivity().startActivity(ConfirmActivity.intent(getContext()));
                getActivity().finish();
            }
        });
    }
}
