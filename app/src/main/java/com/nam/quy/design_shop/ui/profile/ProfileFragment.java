package com.nam.quy.design_shop.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.login.LoginActivity;

import java.util.List;

import javax.inject.Inject;
import android.content.Intent;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProfileFragment extends BaseFragment implements ProfileContract.View {
    @Inject
    ProfilePresenter presenter;

    Unbinder unbinder;

    HistoryOrderAdapter historyOrderAdapter;
    @BindView(R.id.rv_history)
    RecyclerView rvHistory;
    @BindView(R.id.tv_name)
    TextView tvName;
    UserManager userManager;

    @OnClick({R.id.img_back,R.id.img_logout})
    void onClick(View view){
        switch (view.getId()){
            case R.id.img_back:{
                getActivity().onBackPressed();
                break;
            }
            case R.id.img_logout:{
                presenter.logout();
            }
        }
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerProfileComponent.builder()
                .profilePresenterModule(new ProfilePresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
        userManager = new UserManager(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvName.setText(userManager.getUser().name);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void logout() {
        Intent intent=LoginActivity.intent(getContext());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(intent);

    }

    @Override
    public void listOrder(List<Order> orderList) {
        historyOrderAdapter = new HistoryOrderAdapter(getContext(),orderList);
        rvHistory.setAdapter(historyOrderAdapter);
        rvHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        historyOrderAdapter.notifyDataSetChanged();
    }
}
