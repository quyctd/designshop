package com.nam.quy.design_shop.ui.signup;

import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.home.HomeActivity;
import com.nam.quy.design_shop.util.CustomProgressDialog;
import com.nam.quy.design_shop.util.DialogUtil;
import com.nam.quy.design_shop.util.UiUtil;
import com.nam.quy.design_shop.util.Util;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SignUpFragment extends BaseFragment implements SignUpContract.View {

    @Inject
    SignUpPresenter presenter;

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_signup)
    AppCompatButton btnSignUp;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    String name;
    String email;
    String password;

    @OnClick({R.id.btn_signup})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signup: {
                name = edtName.getText().toString().trim();
                email = edtEmail.getText().toString().trim();
                password = edtPassword.getText().toString();
                boolean isNameEmpty = TextUtils.isEmpty(name);
                boolean isEmailEmpty = TextUtils.isEmpty(email);
                boolean isPasswordEmpty = TextUtils.isEmpty(password);

                if (isNameEmpty) {
                    edtName.requestFocus();
                    Util.showMessenger("Vui lòng nhập điền đầy đủ thông tin!", getContext());
                    return;
                }
                if (isEmailEmpty) {
                    edtEmail.requestFocus();
                    Util.showMessenger("Vui lòng nhập điền đầy đủ thông tin!", getContext());
                    return;
                }
                if (isPasswordEmpty) {
                    edtPassword.requestFocus();
                    Util.showMessenger("Vui lòng nhập điền đầy đủ thông tin!", getContext());
                    return;
                }

                if (!UiUtil.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.checkinternet, Toast.LENGTH_SHORT).show();
                    Util.showMessenger("Vui lòng kiểm tra kết nối mạng!", getContext());
                    return;
                }
                customProgressDialog.show();
                presenter.signUp(name, email, password);
                break;
            }
            case R.id.tv_login: {
                getActivity().onBackPressed();
            }
        }
    }

    Unbinder unbinder;

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerSignUpComponent.builder()
                .signUpPresenterModule(new SignUpPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    CustomProgressDialog customProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_frag, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customProgressDialog = DialogUtil.getProgressDialog(getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void onSignUpSuccess() {
        presenter.login(email, password);
    }

    @Override
    public void onSignUpError() {
        customProgressDialog.dismiss();
        Util.showMessenger("Đăng ký không thành công, vui lòng thử lại sau", getContext());

    }

    @Override
    public void onLoginError() {
        customProgressDialog.dismiss();
        Util.showMessenger("Đăng nhập không thành công, vui lòng thử lại sau", getContext());
    }

    @Override
    public void onLoginSuccess() {
        customProgressDialog.dismiss();
        startActivity(HomeActivity.intent(getContext()));
        getActivity().finish();
    }
}
