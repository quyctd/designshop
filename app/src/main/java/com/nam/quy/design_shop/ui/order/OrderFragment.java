package com.nam.quy.design_shop.ui.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.buy.BuyActivity;
import com.nam.quy.design_shop.ui.home.HomeProductAdapter;
import com.nam.quy.design_shop.util.CustomProgressDialog;
import com.nam.quy.design_shop.util.DialogUtil;
import com.nam.quy.design_shop.util.Formatter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderFragment extends BaseFragment implements OrderContract.View, OrderAdapter.OnItemClick {

    @Inject
    OrderPresenter presenter;

    Unbinder unbinder;

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }

    @BindView(R.id.tv_total)
    TextView tvTotal;
    @BindView(R.id.btn_buy_all)
    Button btnBuyAll;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    OrderAdapter orderAdapter;
    List<Order> orders;

    @OnClick(R.id.img_back)
    void onback() {
        getActivity().onBackPressed();
    }

    @OnClick({R.id.btn_buy_all})
    void onClicked(View view) {

        // presenter.buyAll();
        Log.d("123", "onClick:tien hnah dat hang ");
        Intent i = BuyActivity.intent(getContext());
        if (this.orders.size() > 0) {
            i.putExtra("NAME", this.orders.get(0).getNameReceiver());
            i.putExtra("PHONE", this.orders.get(0).getPhone());
            i.putExtra("ADDRESS", this.orders.get(0).getAddress());
            ((OrderActivity) getActivity()).startActivity(i);
            getActivity().finish();
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerOrderComponent.builder()
                .orderPresenterModule(new OrderPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }
    CustomProgressDialog customProgressDialog;
    @Override
    public void onResume() {
        super.onResume();

        customProgressDialog.show();
        presenter.subscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customProgressDialog= DialogUtil.getProgressDialog(getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void onLoadedOrders(List<Order> orders) {
        customProgressDialog.dismiss();
        this.orders = orders;
        orderAdapter = new OrderAdapter(getContext(), this.orders, this);
        rvOrder.setAdapter(orderAdapter);
        rvOrder.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        orderAdapter.notifyDataSetChanged();
        Long total = 0L;
        for (Order order : orders)
            total += Long.parseLong(order.getPrice());
        tvTotal.setText(Formatter.costToVnd(String.valueOf(total)));
        if (orders.size() == 0) {
            btnBuyAll.setEnabled(false);
        } else {
            btnBuyAll.setEnabled(true);
        }
    }

    @Override
    public void deleteOrderSuccess() {

    }

    @Override
    public void deleteOrderFail() {

    }

    @Override
    public void onClick(String id) {
        presenter.deleteOrder(id);
    }
}
