package com.nam.quy.design_shop.ui.product;

import dagger.Module;
import dagger.Provides;

@Module
public class ProductPresenterModule {
    private final ProductContract.View view;

    public ProductPresenterModule(ProductContract.View view) {
        this.view = view;
    }

    @Provides
    ProductContract.View provideTasksContractView() {
        return view;
    }
}