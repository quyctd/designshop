package com.nam.quy.design_shop.data.source;

import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.data.source.remote.response.Response;
import com.nam.quy.design_shop.data.source.remote.response.User;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DSDataSource {
    @POST("api/authenticate")
    @FormUrlEncoded
    Observable<Response<User>> login(@Field("accountId") String accountId, @Field("password") String password);

    @POST("api/user")
    @FormUrlEncoded
    Observable<Response<JsonObject>> signUp(@Field("name") String name, @Field("email") String email, @Field("password") String password,@Field("confirm") String confirm);

    @GET("api/product")
    Observable<Response<List<Product>>> listProducts();

    @POST("api/order")
    @FormUrlEncoded
    Observable<Response<JsonObject>> addOrder(@Field("userId") String userId, @Field("productId") String productId, @Field("number") String number, @Field("url") String url, @Field("svg") String svg);

    @GET("api/total/{id}")
    Observable<Response<List<Order>>> listOrder(@Path("id") String userId);

    @GET("api/order/delete/{id}")
    Observable<Response<JsonObject>> deleteOrder(@Path("id") String orderId);

    @GET("api/order/confirmBuy/{id}")
    Observable<Response<JsonObject>> buyAll(@Path("id") String userId);

    @GET("api/order/updatePending/{id}")
    Observable<Response<JsonObject>> changeOrderStatus(@Path("id") String orderId);

    @POST("api/receiver")
    @FormUrlEncoded
    Observable<Response<JsonObject>> addReceiver(@Field("userId") String userId, @Field("name") String name, @Field("phone") String phone, @Field("address") String address);

    @GET("api/order/history/{id}")
    Observable<Response<List<Order>>> getMyOrder(@Path("id") String id);

    @GET("api/order/imageRecent/{id}")
    Observable<Response<List<Order>>> getAllOrder(@Path("id")String id);
}
