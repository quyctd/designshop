package com.nam.quy.design_shop.ui.design;

import dagger.Module;
import dagger.Provides;

@Module
public class DesignPresenterModule {
    private final DesignContract.View view;

    public DesignPresenterModule(DesignContract.View view) {
        this.view = view;
    }

    @Provides
    DesignContract.View provideTasksContractView() {
        return view;
    }
}
