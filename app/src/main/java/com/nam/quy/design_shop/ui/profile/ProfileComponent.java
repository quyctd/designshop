package com.nam.quy.design_shop.ui.profile;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = ProfilePresenterModule.class)
public interface ProfileComponent {
    void inject(ProfileFragment fragment);
}