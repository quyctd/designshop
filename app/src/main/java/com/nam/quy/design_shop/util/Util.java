package com.nam.quy.design_shop.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.nam.quy.design_shop.App;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Random;

public class Util {
    public static int piToDpi(int i) {
        int k = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, App.getInstant().getContext().getResources().getDisplayMetrics());
        return k;
    }

    public static Bitmap screenshot(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);

        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());

        v.setDrawingCacheEnabled(false);
        return b;

    }

    public static void saveBitmapToImg(Bitmap bitmap) {
        if (bitmap == null) return;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/req_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "myfile.jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            App.getInstant().getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveSVG(String bitmap, Context context) {
        if (bitmap == null) return;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/req_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "svg.svg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {

            String filepath = myDir.getPath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(filepath);
                byte[] buffer = bitmap.getBytes();
                fos.write(buffer, 0, buffer.length);
                fos.close();
            } catch (Exception e) {

            } finally {
                if (fos != null)
                    fos.close();
            }
            Toast.makeText(context, "DONE", Toast.LENGTH_LONG);
            App.getInstant().getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMessenger(String string, Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Thông báo");
        builder.setMessage(string);
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showMessenger(String string, Context context, OnCallBack onCallBack) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Thông báo");
        builder.setMessage(string);
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onCallBack.todo();
                dialogInterface.dismiss();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public interface OnCallBack {
        void todo();
    }

    public static String creatFileName(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyMMdd_hhmss_aaa");
        String fileName = simpleDateFormat.format(System.currentTimeMillis()) + format;
        return fileName;
    }

}
