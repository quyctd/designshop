package com.nam.quy.design_shop.ui.login;

import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class LoginPresenter implements LoginContract.Presenter {
    private final LoginContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    LoginPresenter(LoginContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }
    @Override
    public void login(String username, String password) {
        Disposable loginDisposable = dsRepository.login(username,password)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response ->{
                    Log.i("RES LOGIN", "login: "+response.data);
                    if (!response.isSuccess()) {
                        view.onLoginError();
                        return;
                    }
                    userManager.saveUser(response.data);

                    view.onLoginSuccess();
                },throwable -> {
                    Log.i("RES ERR", "login: "+throwable.getMessage());
                    view.onLoginError();
                });
        compositeDisposable.add(loginDisposable);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
