package com.nam.quy.design_shop.ui.order;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = OrderPresenterModule.class)
public interface OrderComponent {
    void inject(OrderFragment fragment);
}
