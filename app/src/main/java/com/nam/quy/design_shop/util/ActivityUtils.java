package com.nam.quy.design_shop.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.nam.quy.design_shop.R;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * This provides methods to help Activities load their UI.
 */
public class ActivityUtils {

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     */
    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        addFragmentToActivity(fragmentManager, fragment, frameId, true);
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId,
                                             boolean isAddToBackStack) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        if (isAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void startActivityWithAnimation(Context context, Intent intent,
                                                  int resEnterAnimation, int resExitAnimation) {
        Bundle options = ActivityOptionsCompat.makeCustomAnimation(context,
                resEnterAnimation, resExitAnimation).toBundle();
        ActivityCompat.startActivity(context, intent, options);
    }

    public static void startActivityWithAnimationBottomUp(Context context, Intent intent) {
        startActivityWithAnimation(context, intent,
                R.anim.bottom_to_up_slide, android.R.anim.fade_out);
    }

    public static void startActivityWithAnimation(Context context, Intent intent) {
        startActivityWithAnimation(context, intent,
                R.anim.activity_open_enter_slide, R.anim.activity_open_exit_slide);
    }

    public static void startActivityWithAnimation(Activity activity, Intent intent, int requestCode) {
        Bundle options = ActivityOptionsCompat.makeCustomAnimation(activity,
                R.anim.activity_open_enter_slide,
                R.anim.activity_open_exit_slide).toBundle();
        ActivityCompat.startActivityForResult(activity, intent, requestCode, options);
    }

}
