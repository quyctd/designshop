package com.nam.quy.design_shop.ui.product;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;

public interface ProductContract {
    interface View extends BaseView<Presenter>{

    }
    interface Presenter extends BasePresenter{
        void addOrder(String productId,String number);
    }
}
