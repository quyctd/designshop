package com.nam.quy.design_shop;

import android.content.Context;

import com.nam.quy.design_shop.data.source.DSDataSource;
import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.RxBus;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;
import com.nam.quy.design_shop.util.schedulers.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ApplicationModule {

    private final Context context;
    private final BaseSchedulerProvider schedulerProvider;
    private final UserManager userManager;
    private final RxBus bus;

    ApplicationModule(Context context) {
        this.context = context.getApplicationContext();
        schedulerProvider = new SchedulerProvider();
        userManager = new UserManager(context);
        bus = new RxBus();
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    RxBus provideRxBus() {
        return bus;
    }

    @Provides
    BaseSchedulerProvider provideScheduler() {
        return schedulerProvider;
    }

    @Provides
    UserManager provideLoginManager() {
        return userManager;
    }

//    @Provides
//    @Singleton
//    DSRepository provideDSRepository(DSDataSource webservice) {
//        return new DSRepository(webservice);
//    }
}