package com.nam.quy.design_shop.ui.buy;

import android.util.Log;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.ui.home.HomeContract;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BuyPresenter implements BuyContract.Presenter {
    private final String TAG = "ConfirmPresenter";
    private final BuyContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    BuyPresenter(BuyContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void oder(String name, String phone, String address) {
        Disposable buyAllDisposable = dsRepository.addReceiver(userManager.getUser().id, name, phone, address)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response -> {
                    if (response.isSuccess()) {
                        view.oderSuccess(response.message);
                    } else view.oderFail(response.message);
                }, throwable -> {
                    Log.i(TAG, "receiver : " + throwable.getMessage());
                    view.oderFail(throwable.getMessage());
                });
        compositeDisposable.add(buyAllDisposable);
    }
}
