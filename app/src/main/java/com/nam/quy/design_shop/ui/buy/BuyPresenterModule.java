package com.nam.quy.design_shop.ui.buy;

import com.nam.quy.design_shop.ui.home.HomeContract;

import dagger.Module;
import dagger.Provides;

@Module
public class BuyPresenterModule {
    private final BuyContract.View view;

    public BuyPresenterModule(BuyContract.View view) {
        this.view = view;
    }

    @Provides
    BuyContract.View provideTasksContractView() {
        return view;
    }
}