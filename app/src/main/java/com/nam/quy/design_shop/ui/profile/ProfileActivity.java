package com.nam.quy.design_shop.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {
    public static Intent intent(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        ButterKnife.bind(this);
        ProfileFragment profileFragment =
                (ProfileFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (profileFragment == null) {
            profileFragment = ProfileFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), profileFragment, R.id.container);
        }
    }
}
