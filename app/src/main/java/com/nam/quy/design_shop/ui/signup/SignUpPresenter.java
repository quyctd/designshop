package com.nam.quy.design_shop.ui.signup;

import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class SignUpPresenter implements SignUpContract.Presenter {
    private final SignUpContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;


    @Inject
    SignUpPresenter(SignUpContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void signUp(String name, String email, String password) {
        Disposable signUpDisposable = dsRepository.signUp(name,email,password,password)
                                        .subscribeOn(schedulerProvider.io())
                                        .observeOn(schedulerProvider.ui())
                                        .subscribe(response ->{
                                            Log.d("SIGUP", "signUp: "+response.message);
                                            if (!response.isSuccess()) {
                                                view.onSignUpError();
                                                return;
                                            }
                                            //userManager.saveUser(response.data);
                                            view.onSignUpSuccess();
                                        },throwable -> {
                                            Timber.e(throwable);
                                            view.onSignUpSuccess();
                                            Log.d("SIGUP", "signUp: "+throwable.getMessage());
                                        });
        compositeDisposable.add(signUpDisposable);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        compositeDisposable.dispose();
    }

    @Override
    public void login(String username, String password) {
        Disposable loginDisposable = dsRepository.login(username,password)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response ->{
                    Log.i("RES LOGIN", "login: "+response.data);
                    if (!response.isSuccess()) {
                        view.onLoginError();
                        return;
                    }
                    userManager.saveUser(response.data);

                    view.onLoginSuccess();
                },throwable -> {
                    Log.i("RES ERR", "login: "+throwable.getMessage());
                    view.onLoginError();
                });
        compositeDisposable.add(loginDisposable);
    }
}
