package com.nam.quy.design_shop.util;

import android.content.Context;

import com.nam.quy.design_shop.R;


public class DialogUtil {
    public static CustomProgressDialog getProgressDialog(Context context) {
        CustomProgressDialog progressDialog = new CustomProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}
