package com.nam.quy.design_shop.ui.profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.util.Formatter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryOrderAdapter extends RecyclerView.Adapter<HistoryOrderAdapter.ViewHolder> {
    Context context;
    List<Order> listOrders;
    public HistoryOrderAdapter(Context context, List<Order> listOrders) {
        this.context = context;
        this.listOrders = listOrders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_history_order,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Order order = listOrders.get(i);
        Glide
                .with(context)
                .load(order.getUrl())
                .into(viewHolder.imgProduct);
        viewHolder.tvProduct.setText(order.getNameProduct());
        viewHolder.tvPrice.setText(Formatter.costToVnd(order.getPrice()));
        viewHolder.tvInfo.setText(order.getNameReceiver()+" - "+order.getPhone()+" - "+order.getAddress());
    }

    @Override
    public int getItemCount() {
        return listOrders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product)
        TextView tvProduct;
        @BindView(R.id.tv_info)
        TextView tvInfo;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.img_product)
        ImageView imgProduct;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
