package com.nam.quy.design_shop.net;

import android.text.TextUtils;

import com.nam.quy.design_shop.Events;
import com.nam.quy.design_shop.data.source.remote.response.Response;
import com.nam.quy.design_shop.util.RxBus;


/**
 * Created by quydc
 */
public class AlertHelper {
    private final RxBus bus;

    public AlertHelper(RxBus bus) {
        this.bus = bus;
    }

    public static String getMsgForResponse(Response response) {
        if (response == null) {
            return "";
        }
        switch (response.resultCode) {

        }
        return "";
    }

    public void showMsgForResponse(Response response) {
        final String msg = getMsgForResponse(response);
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        show(msg);
    }

    public void show(String msg) {
        bus.send(new Events.MessageEvent(msg));
    }
}
