package com.nam.quy.design_shop;

/**
 * Created by quydc.
 */
public class Events {
    private Events() {
    }

    public static class UnauthorizedEvent {
    }

    public static class MessageEvent {
        public final String msg;

        public MessageEvent(String msg) {
            this.msg = msg;
        }

        @Override
        public String toString() {
            return msg;
        }
    }
}
