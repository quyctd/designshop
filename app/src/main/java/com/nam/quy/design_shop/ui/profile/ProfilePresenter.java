package com.nam.quy.design_shop.ui.profile;

import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ProfilePresenter implements ProfileContract.Presenter {

    private final String TAG = "ProfilePresenter";
    private final ProfileContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    ProfilePresenter(ProfileContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        listOrder();
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.dispose();
    }

    @Override
    public void logout() {
        userManager.logout();
        view.logout();
    }

    @Override
    public void listOrder() {
        Disposable listOrder = dsRepository.getMyOrder(userManager.getUser().id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response ->{
                    if (response.isSuccess()){
                        view.listOrder(response.data);
                    }
                },throwable -> {
                    Log.i(TAG, "listOrder: "+throwable.getMessage());
                });
        compositeDisposable.add(listOrder);
    }
}
