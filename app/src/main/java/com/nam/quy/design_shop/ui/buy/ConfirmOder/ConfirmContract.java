package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;
import com.nam.quy.design_shop.data.source.remote.response.Order;

import java.util.List;

public interface ConfirmContract {
    interface View extends BaseView<Presenter> {
        void onLoadedOrders(List<Order> orders);
        void deleteOrderSuccess();
        void deleteOrderFail();
        void onOderSuccess(String mes);
        void onOderFail(String mes);
    }
    interface Presenter extends BasePresenter {
        void deleteOrder(String id);
        void buyAll();
    }
}
