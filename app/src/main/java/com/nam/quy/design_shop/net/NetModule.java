package com.nam.quy.design_shop.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.nam.quy.design_shop.Events;
import com.nam.quy.design_shop.data.source.DSDataSource;
import com.nam.quy.design_shop.manager.Pref;
import com.nam.quy.design_shop.util.RxBus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by quydc
 */
@Module
public class NetModule {
    private String baseUrl;

    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private static OkHttpClient.Builder getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            X509TrustManager trustManager = (X509TrustManager) trustAllCerts[0];

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, trustManager)
                    .hostnameVerifier((hostname, session) -> true);

            return builder;
        } catch (Exception e) {
            Log.d("", "" + e);
            return null;
            //throw new RuntimeException(e);
        }
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLogging() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Context context,
                                     HttpLoggingInterceptor loggingInterceptor,
                                     RxBus bus, Gson gson, AlertHelper alertHelper) {
        final Interceptor interceptor = chain -> {
            final String defaultTokenAndAppId = "";
            String token = defaultTokenAndAppId;
//            try {
//                token = Pref.getToken(context);
//            } catch (Exception ignored) {
//                Log.d("", "" + ignored);
//            }
            if (token == null) token = defaultTokenAndAppId;

            final Request newRequest = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json; charset=UTF-8")
                    .addHeader("Authorization", String.format("Bearer %s", token))
                    .build();
            Response response = chain.proceed(newRequest);
            if (response.code() == 401) {
                bus.send(new Events.UnauthorizedEvent());
            } else {
                try {
                    ResponseBody responseBody = response.body();
                    BufferedSource source = responseBody.source();
                    source.request(Long.MAX_VALUE);
                    Buffer buffer = source.buffer();
                    String content = buffer.clone().readString(Charset.forName("UTF-8"));

                    com.nam.quy.design_shop.data.source.remote.response.Response res = gson.fromJson(
                            content, com.nam.quy.design_shop.data.source.remote.response.Response.class
                    );
                    alertHelper.showMsgForResponse(res);
                } catch (Exception ignored) {
                    Log.d("", "" + ignored);
                }
            }
            return response;
        };
        return getUnsafeOkHttpClient()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    AlertHelper providesAlertHelper(RxBus bus) {
        return new AlertHelper(bus);
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    DSDataSource provideDSDataSource(Retrofit retrofit) {
        return retrofit.create(DSDataSource.class);
    }
}
