package com.nam.quy.design_shop.ui.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.ui.product.ProductActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeOrderAdapter extends RecyclerView.Adapter<HomeOrderAdapter.ViewHolder> {
    List<Order> listProducts;
    Context context;
    HomePresenter homePresenter;

    public HomeOrderAdapter(Context context, List<Order> listProduts, HomePresenter homePresenter, OnClickItem onClickItem) {
        this.context = context;
        this.listProducts = listProduts;
        this.homePresenter = homePresenter;
        this.onClickItem = onClickItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_home, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Order product = listProducts.get(i);
        Glide.with(context)
                .load(product.getUrl())
                .into(viewHolder.imgProduct);
//        viewHolder.tvProduct.setText(product.getName());
//        viewHolder.tvDesign.setOnClickListener(v -> {
//            //homePresenter.addOrder(product.getId(),"1");
//            Intent i1 = DesignActivity.intent(context);
//
//            i1.putExtra("DATA", product);
//            context.startActivity(i1);
//        });
        viewHolder.tvDesign.setOnClickListener(v -> onClickItem.onBuy(product.getProductId(), product.getUrl(), product.getSvg()));
    }

    OnClickItem onClickItem;

    @Override
    public int getItemCount() {
        return listProducts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_product)
        ImageView imgProduct;
        @BindView(R.id.tv_product)
        TextView tvProduct;
        @BindView(R.id.tv_design)
        TextView tvDesign;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface OnClickItem {
        void onBuy(String productId, String url, String svg);
    }
}
