package com.nam.quy.design_shop.ui.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.util.Formatter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    Context context;
    List<Order> listOrders;
    OnItemClick onItemClick;
    public OrderAdapter(Context context,List<Order> listOrders,OnItemClick onItemClick) {
        this.context = context;
        this.listOrders = listOrders;
        this.onItemClick = onItemClick;
    }

    interface OnItemClick{
        void onClick(String id);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Order order = listOrders.get(i);
        Glide.with(context)
                .load(order.getUrl())
                .into(viewHolder.imgProduct);
        viewHolder.tvProduct.setText(order.getNameProduct());
        if (order.getDescription().length()>80){
            viewHolder.tvDesciprtion.setText(order.getDescription().substring(0,80)+"...");
        }else {
            viewHolder.tvDesciprtion.setText(order.getDescription());
        }

        viewHolder.tvPrice.setText(Formatter.costToVnd(order.getPrice()));
        viewHolder.imgDelete.setOnClickListener(v -> onItemClick.onClick(order.getId()));
    }

    @Override
    public int getItemCount() {
        return listOrders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_product)
        ImageView imgProduct;
        @BindView(R.id.tv_product)
        TextView tvProduct;
        @BindView(R.id.tv_desciprtion)
        TextView tvDesciprtion;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
