package com.nam.quy.design_shop.ui.order;

import dagger.Module;
import dagger.Provides;

@Module
public class OrderPresenterModule {
    private final OrderContract.View view;

    public OrderPresenterModule(OrderContract.View view) {
        this.view = view;
    }

    @Provides
    OrderContract.View provideTasksContractView() {
        return view;
    }
}
