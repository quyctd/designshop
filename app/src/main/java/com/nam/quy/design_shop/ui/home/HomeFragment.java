package com.nam.quy.design_shop.ui.home;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.login.LoginActivity;
import com.nam.quy.design_shop.ui.order.OrderActivity;
import com.nam.quy.design_shop.ui.profile.ProfileActivity;
import com.nam.quy.design_shop.ui.profile.ProfileFragment;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends BaseFragment implements HomeContract.View {
    @Inject
    HomePresenter presenter;
    Unbinder unbinder;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @BindView(R.id.slider)
    SliderLayout sliderLayout;

    @BindView(R.id.rv_newproducts)
    RecyclerView rvNewProducts;
    @BindView(R.id.rv_templates)
    RecyclerView rvTemplates;

    HomeProductAdapter homeProductAdapter;
    List<Product> products;

    @OnClick(R.id.img_profile)
    void click() {
        getActivity().startActivity(ProfileActivity.intent(getContext()));
    }

    @OnClick({R.id.img_order})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_order: {
                startActivity(OrderActivity.intent(getContext()));
            }
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerHomeComponent.builder()
                .homePresenterModule(new HomePresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // presenter.subscribe();
        presenter.listProduct();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void showSlider() {
        ArrayList<String> listUrl = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            listUrl.add("https://res.cloudinary.com/flask-image/image/upload/v1558974065/slider"+ i +".png");
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true);

            //add your extra information
            sliderLayout.addSlider(sliderView);
        }

        // set Slider Transition Animation
        // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Fade);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
    }

    @Override
    public void onLoadedProducts(List<Product> products) {
        this.products = products;
        homeProductAdapter = new HomeProductAdapter(getContext(), this.products, presenter);
        rvTemplates.setAdapter(homeProductAdapter);
        rvTemplates.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        homeProductAdapter.notifyDataSetChanged();
    }

    HomeOrderAdapter myOrderAdapter;
    List<Order> myOrder;

    @Override
    public void onLoadMyOrder(List<Order> orders) {
        this.myOrder = orders;
        if (orders == null) this.myOrder = new ArrayList<>();
        myOrderAdapter = new HomeOrderAdapter(getContext(), this.myOrder, presenter, new HomeOrderAdapter.OnClickItem() {
            @Override
            public void onBuy(String productId, String url, String svg) {
                presenter.addOrder(productId, url, svg);
            }
        });
        rvNewProducts.setAdapter(myOrderAdapter);
        rvNewProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

    }

    HomeOrderAdapter myAllAdapter;
    List<Order> allOrder;

    @Override
    public void onLoadAllOrder(List<Order> orders) {
        this.allOrder = orders;
    }

    @Override
    public void nextAct() {
        getActivity().startActivity(OrderActivity.intent(getContext()));
    }
}
