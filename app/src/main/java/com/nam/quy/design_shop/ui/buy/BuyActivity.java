package com.nam.quy.design_shop.ui.buy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.ui.home.HomeActivity;
import com.nam.quy.design_shop.ui.home.HomeFragment;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class BuyActivity extends BaseActivity {
    public static Intent intent(Context context) {
        Intent intent = new Intent(context, BuyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        ButterKnife.bind(this);
        BuyFragment buyFragment =
                (BuyFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (buyFragment == null) {
            buyFragment = BuyFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), buyFragment, R.id.container);
        }

        buyFragment.setup(getIntent().getStringExtra("NAME") == null ? "" : getIntent().getStringExtra("NAME"),
                getIntent().getStringExtra("PHONE") == null ? "" : getIntent().getStringExtra("PHONE"),
                getIntent().getStringExtra("ADDRESS") == null ? "" : getIntent().getStringExtra("ADDRESS"));

    }
}
