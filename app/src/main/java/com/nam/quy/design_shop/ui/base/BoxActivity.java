package com.nam.quy.design_shop.ui.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.util.ActivityUtils;

import java.io.Serializable;

import androidx.annotation.Nullable;

/**
 * Created by quydc
 */
public class BoxActivity extends BaseActivity {
    public static Intent getCallingIntent(Context context, FragmentFactory factory) {
        Intent intent = new Intent(context, BoxActivity.class);
        intent.putExtra("factory", factory);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        FragmentFactory factory = (FragmentFactory) getIntent().getSerializableExtra("factory");
        if (savedInstanceState == null && factory != null) {
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    factory.create(),
                    R.id.container);
        }
    }

    public interface FragmentFactory extends Serializable {
        Fragment create();
    }
}
