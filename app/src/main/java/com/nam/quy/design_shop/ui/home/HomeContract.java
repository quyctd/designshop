package com.nam.quy.design_shop.ui.home;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.data.source.remote.response.Product;

import java.util.ArrayList;
import java.util.List;

public interface HomeContract {
    interface View extends BaseView<Presenter> {
        void showSlider();

        void onLoadedProducts(List<Product> products);

        void onLoadMyOrder(List<Order> orders
        );

        void onLoadAllOrder(List<Order> orders
        );


        void nextAct();
    }

    interface Presenter extends BasePresenter {
        void listProduct();

        void getMyOrder();

        void getAllOrder();

        void addOrder(String productId, String number,String svg);
    }
}
