package com.nam.quy.design_shop.ui.buy;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;

public interface BuyContract {
    interface View extends BaseView<Presenter> {
        void oderFail(String text);

        void oderSuccess(String text);
    }

    interface Presenter extends BasePresenter {
        void oder(String name, String phone, String address);
    }
}