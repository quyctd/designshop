package com.nam.quy.design_shop.ui.base;

import android.os.Bundle;


import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import androidx.annotation.Nullable;

/**
 * Created by quydc
 */
public abstract class BaseActivity extends RxAppCompatActivity {
    public static final String ARG_RES_ANI_ENTER = "ARG_RES_ANI_ENTER";
    public static final String ARG_RES_ANI_EXIT = "ARG_RES_ANI_EXIT";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
            return;
        }
        super.onBackPressed();
    }
}
