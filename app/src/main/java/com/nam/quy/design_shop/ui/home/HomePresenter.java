package com.nam.quy.design_shop.ui.home;


import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class HomePresenter implements HomeContract.Presenter {

    private final String TAG = "ConfirmPresenter";
    private final HomeContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    HomePresenter(HomeContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        view.showSlider();

        getMyOrder();
        getAllOrder();
    }

    @Override
    public void unsubscribe() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public void listProduct() {
        Disposable listProdictDisposable = dsRepository.listProducts()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(listResponse -> {
                    view.onLoadedProducts(listResponse.data);
                }, throwable -> {
                    Log.i(TAG, "listProduct: " + throwable.toString());
                });
        compositeDisposable.add(listProdictDisposable);
    }

    @Override
    public void getMyOrder() {
        Disposable listProdictDisposable = dsRepository.getMyOrder(userManager.getUser().id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(listResponse -> {
                    Log.d(TAG, "getMyOrder:SUC " + listResponse.data);
                    if (listResponse.isSuccess()) view.onLoadMyOrder(listResponse.data);
                }, throwable -> {
                    Log.i(TAG, "getMyOrder:ERR " + throwable.toString());
                });
        compositeDisposable.add(listProdictDisposable);
    }

    @Override
    public void getAllOrder() {
        Disposable listProdictDisposable = dsRepository.getAllOrder(userManager.getUser().id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(listResponse -> {
                    Log.d(TAG, "getMyOrder:SUC " + listResponse.data);
                    if (listResponse.isSuccess()) view.onLoadAllOrder(listResponse.data);
                }, throwable -> {
                    Log.i(TAG, "getMyOrder:ERR " + throwable.toString());
                });
        compositeDisposable.add(listProdictDisposable);
    }

    @Override
    public void addOrder(String productId, String url,String svg) {
        Disposable getOrdersDisposable = dsRepository.addOrder(userManager.getUser().id, productId, "1", url,svg)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(jsonObjectResponse -> {
                    Timber.tag(TAG).i("getOrders: %s", jsonObjectResponse.data);

                    if (jsonObjectResponse.isSuccess()) {
                        //view.nextAct();
                    }
                    view.nextAct();

                }, throwable -> {
                    Log.d(TAG, "onOder: " + throwable.getMessage());
                    Timber.tag(TAG).e(throwable);
                });
        compositeDisposable.add(getOrdersDisposable);
    }


}
