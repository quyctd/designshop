package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.buy.BuyActivity;
import com.nam.quy.design_shop.ui.order.DaggerOrderComponent;
import com.nam.quy.design_shop.util.CustomProgressDialog;
import com.nam.quy.design_shop.util.DialogUtil;
import com.nam.quy.design_shop.util.Formatter;
import com.nam.quy.design_shop.util.Util;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ConfirmFragment extends BaseFragment implements ConfirmContract.View, ConfirmAdapter.OnItemClick {

    @Inject
    ConfirmPresenter presenter;

    Unbinder unbinder;

    public static ConfirmFragment newInstance() {
        ConfirmFragment fragment = new ConfirmFragment();
        return fragment;
    }

    @BindView(R.id.tv_total)
    TextView tvTotal;
    @BindView(R.id.btn_buy_all)
    Button btnBuyAll;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    ConfirmAdapter orderAdapter;
    List<Order> orders;

    @OnClick(R.id.img_back)
    void onback() {
        getActivity().onBackPressed();
    }

    @OnClick({R.id.btn_buy_all})
    void onClicked(View view) {
        customProgressDialog.show();
        presenter.buyAll();


    }

    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_address)
    TextView txtAdd;
    @BindView(R.id.txt_phone)
    TextView txtPhone;

    void todo() {

        if (this.orders.size() > 0) {
            txtName.setText(this.orders.get(0).getNameReceiver());
            txtAdd.setText(this.orders.get(0).getPhone());
            txtPhone.setText(this.orders.get(0).getAddress());
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerConfirmComponent.builder()
                .confirmPresenterModule(new ConfirmPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirm_oder_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    CustomProgressDialog customProgressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customProgressDialog = DialogUtil.getProgressDialog(getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    @Override
    public void onLoadedOrders(List<Order> orders) {
        this.orders = orders;
        orderAdapter = new ConfirmAdapter(getContext(), this.orders, this);
        rvOrder.setAdapter(orderAdapter);
        rvOrder.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        orderAdapter.notifyDataSetChanged();
        Long total = 0L;
        for (Order order : orders)
            total += Long.parseLong(order.getPrice());
        tvTotal.setText(Formatter.costToVnd(String.valueOf(total)));
        if (orders.size() == 0) {
            btnBuyAll.setEnabled(false);
        } else {
            btnBuyAll.setEnabled(true);
        }
        todo();
    }

    @Override
    public void deleteOrderSuccess() {

    }

    @Override
    public void deleteOrderFail() {

    }

    @Override
    public void onOderSuccess(String mes) {
        customProgressDialog.dismiss();
        Util.showMessenger("Đơn hàng của bạn đã được tiếp nhận, chúng tôi sẽ xử lý trong thời gian sớm nhất. Xin cảm ơn!", getContext(), new Util.OnCallBack() {
            @Override
            public void todo() {
                getActivity().finish();
            }
        });
    }

    @Override
    public void onOderFail(String mes) {
        customProgressDialog.dismiss();
        Util.showMessenger("Không thành công, vui lòng thử lại!", getContext(), new Util.OnCallBack() {
            @Override
            public void todo() {
                //getActivity().finish();
            }
        });
    }

    @Override
    public void onClick(String id) {
        presenter.deleteOrder(id);
    }
}
