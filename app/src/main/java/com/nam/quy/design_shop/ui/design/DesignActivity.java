package com.nam.quy.design_shop.ui.design;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.ui.buy.BuyActivity;
import com.nam.quy.design_shop.ui.buy.BuyFragment;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class DesignActivity extends BaseActivity {
    public static Intent intent(Context context) {
        Intent intent = new Intent(context, DesignActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            product = (Product) getIntent().getSerializableExtra("DATA"); //Obtaining data
        }
        DesignFragment buyFragment =
                (DesignFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (buyFragment == null) {
            buyFragment = DesignFragment.newInstance(product);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), buyFragment, R.id.container);
        }
    }
}
