package com.nam.quy.design_shop.ui.buy;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;
import com.nam.quy.design_shop.ui.buy.ConfirmOder.ConfirmFragment;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = BuyPresenterModule.class)
public interface BuyComponent {
    void inject(BuyFragment fragment);


}