package com.nam.quy.design_shop.ui.design;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemView> {

    List<String> arr;
    Context context;

    public ItemAdapter(List<String> arr, Context context, OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
        this.context = context;
        this.arr = arr;
    }

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_rscv, parent, false);
        return new ItemView(view);
    }

    @Override
    public void onBindViewHolder(final ItemView holder, final int position) {
        Log.d("AAAA", position + "onBindViewHolder: " + arr.get(position));

//        Picasso.get()
//                .load(arr.get(position)).resize(50, 50)
//                .centerCrop()
//                .placeholder(R.drawable.save)
//                .error(R.drawable.frame)
//                .into(holder.imageView);

        Glide
                .with(context)
                .load(arr.get(position))
                .into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onclick(position);
            }
        });

    }

    OnItemClick onItemClick;

    public interface OnItemClick {
        void onclick(int position);

    }

    @Override
    public int getItemCount() {
        if (arr == null) return 0;
        return arr.size();
    }

    public class ItemView extends RecyclerView.ViewHolder {
        @BindView(R.id.img_product)
        ImageView imageView;

        public ItemView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
