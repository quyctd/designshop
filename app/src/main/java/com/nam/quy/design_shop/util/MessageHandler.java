package com.nam.quy.design_shop.util;

import android.content.Context;
import android.widget.Toast;

import com.nam.quy.design_shop.Events;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by quydc
 */
public class MessageHandler {
    private final RxBus bus;
    private final Context context;
    private Disposable showMsgDisposable;

    public MessageHandler(Context context, RxBus bus) {
        this.context = context;
        this.bus = bus;
    }

    private void unregister() {
        if (showMsgDisposable != null) {
            showMsgDisposable.dispose();
            showMsgDisposable = null;
        }
    }

    public void register() {
        if (showMsgDisposable != null) {
            return;
        }
        showMsgDisposable = bus
                .asFlowable()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(obj -> obj instanceof Events.MessageEvent)
                .subscribe(
                        obj -> {
                            Toast.makeText(context, String.valueOf(obj), Toast.LENGTH_SHORT).show();
                        },
                        throwable -> Timber.d(throwable, "error when handle bus")
                );
    }
}
