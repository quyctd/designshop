package com.nam.quy.design_shop.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by quydc.
 */
public class Formatter {
    private static SafeFormatDate safeFormatFullDateTime = new SafeFormatDate(new SimpleDateFormat("dd/MM/yyyy hh:mm"));
    private static SafeFormatDate safeFormatDateTime = new SafeFormatDate(new SimpleDateFormat("dd/MM/yyyy"));

    private Formatter() {

    }

    public static String formatCost(long cost) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);

        return formatter.format(cost);
    }

    public static String costToVnd(int cost) {
        return formatCost(cost) + " VNĐ";
    }

    public static String costToVnd(String cost) {
        if (cost == null || cost.length() == 0 || cost.isEmpty()) {
            return "";
        }
        if (cost.length() > 10) {
            cost = cost.substring(0, 9);
        }
        return formatCost(Long.valueOf(cost)) + " VNĐ";
    }

    public static String costToVndMultiLine(int cost) {
        return formatCost(cost) + "\nVNĐ";
    }

    public static String formatFullDateTime(long time) {
        return safeFormatFullDateTime.get().format(new Date(time));
    }


    public static String formatDateTime(long time) {
        return safeFormatDateTime.get().format(new Date(time));
    }

    public static long formatStringToDateTime(String time) throws ParseException {
        return safeFormatDateTime.get().parse(time).getTime();
    }

    private static class SafeFormatDate {
        private final ThreadLocal<SimpleDateFormat> simpleDateFormatThreadLocal;

        private SafeFormatDate(SimpleDateFormat format) {
            simpleDateFormatThreadLocal = new ThreadLocal<SimpleDateFormat>() {
                @Override
                protected SimpleDateFormat initialValue() {
                    return format;
                }
            };
        }

        public SimpleDateFormat get() {
            return simpleDateFormatThreadLocal.get();
        }

    }
}
