package com.nam.quy.design_shop.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.home.HomeActivity;
import com.nam.quy.design_shop.ui.signup.SignUpActivity;
import com.nam.quy.design_shop.util.CustomProgressDialog;
import com.nam.quy.design_shop.util.DialogUtil;
import com.nam.quy.design_shop.util.UiUtil;
import com.nam.quy.design_shop.util.Util;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends BaseFragment implements LoginContract.View {
    @Inject
    LoginPresenter presenter;

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.tv_signup)
    TextView tvSignUp;

    Unbinder unbinder;

    @OnClick({R.id.btn_login, R.id.tv_signup})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login: {
                String email = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString();
                boolean isEmailEmpty = TextUtils.isEmpty(email);
                boolean isPasswordEmpty = TextUtils.isEmpty(password);

                if (isEmailEmpty) {
                    edtEmail.requestFocus();
                    return;
                }
                if (isPasswordEmpty) {
                    edtPassword.requestFocus();
                    return;
                }

                if (!UiUtil.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.checkinternet, Toast.LENGTH_SHORT).show();
                    return;
                }
                customProgressDialog.show();
                presenter.login(email, password);
                //( (LoginActivity)getActivity()).onLoginSuccess();
                break;
            }
            case R.id.tv_signup: {
                startActivity(SignUpActivity.intent(getContext()));
            }
        }
    }


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerLoginComponent.builder()
                .loginPresenterModule(new LoginPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_frag, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    CustomProgressDialog customProgressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customProgressDialog = DialogUtil.getProgressDialog(getContext());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }


    @Override
    public void onLoginSuccess() {
        customProgressDialog.dismiss();
        startActivity(HomeActivity.intent(getContext()));
        getActivity().finish();
    }

    @Override
    public void onLoginError() {
        customProgressDialog.dismiss();
        Util.showMessenger("Không thành công, vui lòng thử lại!", getContext());
    }
}
