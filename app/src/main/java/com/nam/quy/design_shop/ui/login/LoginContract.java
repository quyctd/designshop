package com.nam.quy.design_shop.ui.login;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;

public interface LoginContract {

    interface View extends BaseView<Presenter> {
        void onLoginSuccess();

        void onLoginError();
    }

    interface Presenter extends BasePresenter {
        void login(String username, String password);
    }
}
