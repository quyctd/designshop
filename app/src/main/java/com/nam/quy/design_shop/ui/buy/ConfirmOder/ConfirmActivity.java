package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class ConfirmActivity extends BaseActivity {
    public static Intent intent(Context context) {
        Intent intent = new Intent(context, ConfirmActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_act);
        ButterKnife.bind(this);
        ConfirmFragment orderFragment =
                (ConfirmFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (orderFragment == null) {
            orderFragment = ConfirmFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), orderFragment, R.id.container);
        }
    }


}
