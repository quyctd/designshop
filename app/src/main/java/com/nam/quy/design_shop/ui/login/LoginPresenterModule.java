package com.nam.quy.design_shop.ui.login;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginPresenterModule {
    private final LoginContract.View view;

    public LoginPresenterModule(LoginContract.View view) {
        this.view = view;
    }

    @Provides
    LoginContract.View provideTasksContractView() {
        return view;
    }
}
