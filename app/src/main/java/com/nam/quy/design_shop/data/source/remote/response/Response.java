package com.nam.quy.design_shop.data.source.remote.response;

import com.google.gson.annotations.SerializedName;

public class Response<T> {
    @SerializedName("message")
    public String message;
    @SerializedName("code")
    public int resultCode;

    @SerializedName("data")
    public T data;

    public boolean isSuccess() {
        return resultCode >= 200 && resultCode < 300;
    }
}