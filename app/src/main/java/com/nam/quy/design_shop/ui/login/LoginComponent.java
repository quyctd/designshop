package com.nam.quy.design_shop.ui.login;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = LoginPresenterModule.class)
public interface LoginComponent {
    void inject(LoginFragment fragment);
}
