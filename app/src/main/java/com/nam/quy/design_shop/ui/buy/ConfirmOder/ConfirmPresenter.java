package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ConfirmPresenter implements ConfirmContract.Presenter {
    private final String TAG = "ConfirmPresenter";
    private final ConfirmContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    ConfirmPresenter(ConfirmContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        getOrders(userManager.getUser().id);
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.dispose();
    }

    void getOrders(String userId) {
        Disposable getOrdersDisposable = dsRepository.listOrder(userId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(jsonObjectResponse -> {
                    view.onLoadedOrders(jsonObjectResponse.data);
                }, throwable -> {
                    Log.i(TAG, "getOrders: " + throwable.getMessage());
                });
        compositeDisposable.add(getOrdersDisposable);
    }

    @Override
    public void deleteOrder(String id) {
        Disposable deleteDisposable = dsRepository.deleteOrder(id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response -> {
                    getOrders(userManager.getUser().id);
                    view.deleteOrderSuccess();
                }, throwable -> {
                    view.deleteOrderFail();
                    Log.i(TAG, "deleteOrders: " + throwable.getMessage());
                });
        compositeDisposable.add(deleteDisposable);
    }

    @Override
    public void buyAll() {
        Disposable buyAllDisposable = dsRepository.buyAll(userManager.getUser().id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response -> {
                    if (response.isSuccess()) view.onOderSuccess(response.message);
                    else view.onOderFail(response.message);
                }, throwable -> {
                    view.onOderFail(throwable.getMessage());
                    Log.i(TAG, "buyAll : " + throwable.getMessage());
                });
        compositeDisposable.add(buyAllDisposable);
    }
}
