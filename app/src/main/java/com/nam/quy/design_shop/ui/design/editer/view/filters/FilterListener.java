package com.nam.quy.design_shop.ui.design.editer.view.filters;


import com.nam.quy.design_shop.ui.design.customerview.PhotoFilter;

public interface FilterListener {
    void onFilterSelected(PhotoFilter photoFilter);
}