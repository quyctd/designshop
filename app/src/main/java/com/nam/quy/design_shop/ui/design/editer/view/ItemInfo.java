package com.nam.quy.design_shop.ui.design.editer.view;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemInfo implements Serializable {


    @SerializedName("width")
    @Expose
    private int width;

    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("right")
    @Expose
    private int right;
    @SerializedName("left")
    @Expose
    private int left;
    @SerializedName("top")
    @Expose
    private int top;
    @SerializedName("bottom")
    @Expose
    private int bottom;

    @Override
    public String toString() {
        return "ItemInfo{" +
                "width=" + width +
                ", height=" + height +
                ", url=" + url +
                ", right=" + right +
                ", left=" + left +
                ", top=" + top +
                ", bottom=" + bottom +
                '}';
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }
}
