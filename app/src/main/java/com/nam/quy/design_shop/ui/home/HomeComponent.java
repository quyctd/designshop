package com.nam.quy.design_shop.ui.home;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = HomePresenterModule.class)
public interface HomeComponent {
    void inject(HomeFragment fragment);
}