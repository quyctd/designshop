package com.nam.quy.design_shop.ui.design;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.design.customerview.OnPhotoEditorListener;
import com.nam.quy.design_shop.ui.design.customerview.PhotoEditor;
import com.nam.quy.design_shop.ui.design.customerview.PhotoEditorView;
import com.nam.quy.design_shop.ui.design.customerview.PhotoFilter;
import com.nam.quy.design_shop.ui.design.customerview.SaveSettings;
import com.nam.quy.design_shop.ui.design.customerview.ViewType;
import com.nam.quy.design_shop.ui.design.editer.view.EditImageActivity;
import com.nam.quy.design_shop.ui.design.editer.view.EmojiBSFragment;
import com.nam.quy.design_shop.ui.design.editer.view.ItemInfo;
import com.nam.quy.design_shop.ui.design.editer.view.PropertiesBSFragment;
import com.nam.quy.design_shop.ui.design.editer.view.StickerBSFragment;
import com.nam.quy.design_shop.ui.design.editer.view.TextEditorDialogFragment;
import com.nam.quy.design_shop.ui.design.editer.view.base.BaseActivity;
import com.nam.quy.design_shop.ui.design.editer.view.filters.FilterListener;
import com.nam.quy.design_shop.ui.design.editer.view.filters.FilterViewAdapter;
import com.nam.quy.design_shop.ui.design.editer.view.tools.EditingToolsAdapter;
import com.nam.quy.design_shop.ui.design.editer.view.tools.ToolType;
import com.nam.quy.design_shop.ui.order.OrderActivity;
import com.nam.quy.design_shop.util.CustomProgressDialog;
import com.nam.quy.design_shop.util.DialogUtil;
import com.nam.quy.design_shop.util.ImageTracerAndroid;
import com.nam.quy.design_shop.util.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class DesignFragment extends BaseFragment implements DesignContract.View, OnPhotoEditorListener,
        View.OnClickListener,
        PropertiesBSFragment.Properties,
        EmojiBSFragment.EmojiListener,
        StickerBSFragment.StickerListener, EditingToolsAdapter.OnItemSelected, FilterListener {
    @Inject
    DesignPresenter presenter;

    Unbinder unbinder;
    private static final String TAG = EditImageActivity.class.getSimpleName();
    public static final String EXTRA_IMAGE_PATHS = "extra_image_paths";
    private static final int CAMERA_REQUEST = 52;
    private static final int CAMERA_REQUEST_IMG = 54;
    private static final int PICK_REQUEST = 53;
    private static final int PICK_REQUEST_IMG = 55;
    private PhotoEditor mPhotoEditor;
    private PhotoEditorView mPhotoEditorView;
    private PropertiesBSFragment mPropertiesBSFragment;
    private EmojiBSFragment mEmojiBSFragment;
    private StickerBSFragment mStickerBSFragment;
    private TextView mTxtCurrentTool;
    private Typeface mWonderFont;
    private RecyclerView mRvTools, mRvFilters;
    private EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this);
    private FilterViewAdapter mFilterViewAdapter = new FilterViewAdapter(this);
    private ConstraintLayout mRootView;
    private ConstraintSet mConstraintSet = new ConstraintSet();
    private boolean mIsFilterVisible;
    HashMap<String, ItemInfo> info = new HashMap<>();
    RelativeLayout lyMainEdit;
    public Product product;

    public static DesignFragment newInstance(Product product) {
        DesignFragment fragment = new DesignFragment();
        fragment.product = product;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerDesignComponent.builder()
                .designPresenterModule(new DesignPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_image, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customProgressDialog = DialogUtil.getProgressDialog(getContext());
        initViews(view);
        lyMainEdit = view.findViewById(R.id.ly_main_edit);
        mWonderFont = Typeface.createFromAsset(getActivity().getAssets(), "beyond_wonderland.ttf");

        mPropertiesBSFragment = new PropertiesBSFragment();
        mEmojiBSFragment = new EmojiBSFragment();
        mStickerBSFragment = new StickerBSFragment();
        mStickerBSFragment.setStickerListener(this);
        mEmojiBSFragment.setEmojiListener(this);
        mPropertiesBSFragment.setPropertiesChangeListener(this);

        LinearLayoutManager llmTools = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRvTools.setLayoutManager(llmTools);
        mRvTools.setAdapter(mEditingToolsAdapter);

        LinearLayoutManager llmFilters = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(mFilterViewAdapter);


        //Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.roboto_medium);
        //Typeface mEmojiTypeFace = Typeface.createFromAsset(getAssets(), "emojione-android.ttf");

        mPhotoEditor = new PhotoEditor.Builder(getContext(), mPhotoEditorView)
                .setPinchTextScalable(true) // set flag to make text scalable when pinch
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);

        //Set Image Dynamically
        // mPhotoEditorView.getSource().setImageResource(R.drawable.color_palette);


        init(view);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

    void screenshot() {
        //  showSnackbar("SAVE NOW");


        Bitmap bmp = Util.screenshot(lyMainEdit);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        String svg = saveSVG(bmp);
        uploadImg(byteArray, svg.getBytes());

        bmp.recycle();


    }

    String saveSVG(Bitmap bitmap) {
        Log.d(TAG, "saveSVG: ");
        final HashMap<String, Float[]> optionsrange = new HashMap<String, Float[]>();
        optionsrange.put("ltres", new Float[]{1f, 0f, 10f});
        optionsrange.put("qtres", new Float[]{1f, 0f, 10f});
        optionsrange.put("pathomit", new Float[]{8f, 0f, 64f});// int
        optionsrange.put("colorsampling", new Float[]{1f, 0f, 1f}); // 0 = off else on
        optionsrange.put("numberofcolors", new Float[]{16f, 2f, 64f});// int
        optionsrange.put("mincolorratio", new Float[]{0.02f, 0f, 0.1f});
        optionsrange.put("colorquantcycles", new Float[]{3f, 1f, 10f});// int
        optionsrange.put("scale", new Float[]{1f, 0.01f, 100f});
        optionsrange.put("roundcoords", new Float[]{1f, 0f, 8f});
        optionsrange.put("blurradius", new Float[]{0f, 0f, 5f});
        optionsrange.put("blurdelta", new Float[]{20f, 0f, 255f});


        try {
            String url = ImageTracerAndroid.imageToSVG(bitmap, null, null);
            Log.i("URLSVG:", url);
            return url;
            // Util.saveSVG(url, getContext());

        } catch (Exception e) {
            Log.i("URLSVG:", "NULL");
            return "";
        }
    }

    private void init(View view) {
        ImageView img = view.findViewById(R.id.img);
        LinearLayout lnImg = view.findViewById(R.id.lnImg);
        //Object s = getIntent().getSerializableExtra("DATA");


//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            product = (Product) getIntent().getSerializableExtra("DATA"); //Obtaining data
//        }
        Log.d(TAG, "EDITIMG" + product);
        if (product == null) Log.d(TAG, "init: productnull");

        Log.d("PRODUCT", product.toString());
        // Glide.with(this).load(product.getImage()).into(img);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lnImg.getLayoutParams();

        params.setMargins(Util.piToDpi(product.getItemInfo().getLeft()), Util.piToDpi(product.getItemInfo().getTop()),
                Util.piToDpi(product.getItemInfo().getRight()), Util.piToDpi(product.getItemInfo().getBottom()));
        params.height = Util.piToDpi(product.getItemInfo().getHeight());
        params.width = Util.piToDpi(product.getItemInfo().getWidth());
//        lnImg.setLayoutParams(params);
        Glide.with(this).load(product.getItemInfo().getUrl()).into(img);
        lnImg.setLayoutParams(params);
        switch ("aophongtrangnam") {
            case "tui":

                params.setMargins(0, Util.piToDpi(60), 0, 0);
                params.height = Util.piToDpi(180);
                params.width = Util.piToDpi(180);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.tuixach).into(img);
                break;
            case "aophongtrangnam":
                Log.d("EDITIMG", "init: " + Util.piToDpi(0));
                // params.setMargins(0, 0, 0, 0);
                //  params.height = Util.piToDpi(250);
                //  params.width = Util.piToDpi(150);
                lnImg.setLayoutParams(params);
                // Glide.with(this).load(R.drawable.ts).into(img);
                break;
            case "mutrang":
                params.setMargins(0, 0, 0, 40);
                params.height = Util.piToDpi(160);
                params.width = Util.piToDpi(140);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.mutrang).into(img);


                break;
            case "hoob":

                params.setMargins(0, 0, 0, 60);
                params.height = Util.piToDpi(190);
                params.width = Util.piToDpi(140);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.hoob).into(img);
                break;

            case "aoni":

                params.setMargins(0, 0, 0, 20);
                params.height = Util.piToDpi(200);
                params.width = Util.piToDpi(150);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.aoni).into(img);
                break;
            case "ao3lonu":

                params.setMargins(0, 0, 0, 20);
                params.height = Util.piToDpi(200);
                params.width = Util.piToDpi(150);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.ao3lonu).into(img);
                break;
            case "aocotimnu":

                params.setMargins(0, 0, -20, 20);
                params.height = Util.piToDpi(180);
                params.width = Util.piToDpi(150);
                lnImg.setLayoutParams(params);
                Glide.with(this).load(R.drawable.ao3lonu).into(img);
                break;
            default:
                getActivity().finish();
                break;
        }

    }

    private void initViews(View view) {
        ImageView imgUndo;
        ImageView imgRedo;
        ImageView imgCamera;
        ImageView imgGallery;
        ImageView imgSave;
        ImageView imgClose;

        mPhotoEditorView = view.findViewById(R.id.photoEditorView);
        mTxtCurrentTool = view.findViewById(R.id.txtCurrentTool);
        mRvTools = view.findViewById(R.id.rvConstraintTools);
        mRvFilters = view.findViewById(R.id.rvFilterView);
        mRootView = view.findViewById(R.id.rootView);

        imgUndo = view.findViewById(R.id.imgUndo);
        imgUndo.setOnClickListener(this);

        imgRedo = view.findViewById(R.id.imgRedo);
        imgRedo.setOnClickListener(this);

        imgCamera = view.findViewById(R.id.imgCamera);
        imgCamera.setOnClickListener(this);

        imgGallery = view.findViewById(R.id.imgGallery);
        imgGallery.setOnClickListener(this);

        imgSave = view.findViewById(R.id.imgSave);
        imgSave.setOnClickListener(this);

        imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

    }

    @Override
    public void onEditTextChangeListener(final View rootView, String text, int colorCode) {
        TextEditorDialogFragment textEditorDialogFragment =
                TextEditorDialogFragment.show((AppCompatActivity) getActivity(), text, colorCode);
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                mPhotoEditor.editText(rootView, inputText, colorCode);
                mTxtCurrentTool.setText(R.string.label_text);
            }
        });
    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onAddViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {
        Log.d(TAG, "onRemoveViewListener() called with: numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onRemoveViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStartViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStopViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgUndo:
                mPhotoEditor.undo();
                break;

            case R.id.imgRedo:
                mPhotoEditor.redo();
                break;

            case R.id.imgSave:
                saveImage();
                break;

            case R.id.imgClose:
                getActivity().onBackPressed();
                break;

            case R.id.imgCamera:
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;

            case R.id.imgGallery:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST);
                break;
        }
    }

    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{permission},
                    READ_WRITE_STORAGE);
        }
        return isGranted;
    }

    public static final int READ_WRITE_STORAGE = 52;
    private ProgressDialog mProgressDialog;

    protected void showLoading(@NonNull String message) {
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @BindView(R.id.lnImg)
    LinearLayout bgEditer;

    @SuppressLint("MissingPermission")
    private void saveImage() {
        mPhotoEditor.clearHelperBox();
        bgEditer.setBackgroundResource(R.drawable.bg_draw_none);
        customProgressDialog.show();

        screenshot();
        if (true) return;
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showLoading("Saving...");
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + ""
                    + System.currentTimeMillis() + ".png");
            try {
                file.createNewFile();

                SaveSettings saveSettings = new SaveSettings.Builder()
                        .setClearViewsEnabled(true)
                        .setTransparencyEnabled(true)
                        .build();

                mPhotoEditor.saveAsFile(file.getAbsolutePath(), saveSettings, new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        hideLoading();
                        Toast.makeText(getContext(), "Image Saved local Successfully", Toast.LENGTH_SHORT);
                        mPhotoEditorView.getSource().setImageURI(Uri.fromFile(new File(imagePath)));

                        Log.d(TAG, "onSuccess: " + imagePath);

                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        hideLoading();
                        Toast.makeText(getContext(), "Failed to save Image", Toast.LENGTH_SHORT);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                hideLoading();
                //showSnackbar(e.getMessage());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST:
                    mPhotoEditor.clearAllViews();
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    mPhotoEditorView.getSource().setImageBitmap(photo);
                    break;
                case PICK_REQUEST:
                    try {
                        mPhotoEditor.clearAllViews();
                        Uri uri = data.getData();
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                        mPhotoEditorView.getSource().setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case CAMERA_REQUEST_IMG:
                    // mPhotoEditor.clearAllViews();
                    Bitmap photo2 = (Bitmap) data.getExtras().get("data");
                    mPhotoEditor.addImage(photo2);
                    break;
                case PICK_REQUEST_IMG:
                    try {
                        // mPhotoEditor.clearAllViews();
                        Uri uri = data.getData();
                        Bitmap bitmap2 = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                        mPhotoEditor.addImage(bitmap2);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onColorChanged(int colorCode) {
        mPhotoEditor.setBrushColor(colorCode);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onOpacityChanged(int opacity) {
        mPhotoEditor.setOpacity(opacity);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onBrushSizeChanged(int brushSize) {
        mPhotoEditor.setBrushSize(brushSize);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onEmojiClick(String emojiUnicode) {
        mPhotoEditor.addEmoji(emojiUnicode);
        mTxtCurrentTool.setText(R.string.label_emoji);

    }

    @Override
    public void onStickerClick(Bitmap bitmap) {
        mPhotoEditor.addImage(bitmap);
        mTxtCurrentTool.setText(R.string.label_sticker);


    }

    private void showSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you want to exit without saving image ?");

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveImage();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
        builder.create().show();

    }


    @Override
    public void onToolSelected(ToolType toolType) {
        switch (toolType) {
            case BRUSH:
                mPhotoEditor.setBrushDrawingMode(true);
                mTxtCurrentTool.setText(R.string.label_brush);
                mPropertiesBSFragment.show(getActivity().getSupportFragmentManager(), mPropertiesBSFragment.getTag());
                break;
            case TEXT:
                TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show((AppCompatActivity) getActivity());
                textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                    @Override
                    public void onDone(String inputText, int colorCode) {
                        mPhotoEditor.addText(inputText, colorCode);
                        mTxtCurrentTool.setText(R.string.label_text);
                    }
                });
                break;
            case ERASER:
                mPhotoEditor.brushEraser();
                mTxtCurrentTool.setText(R.string.label_eraser);
                break;
            case CAMERA:
                mTxtCurrentTool.setText(R.string.camera);
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_IMG);
                break;


            case IMAGE:
                mTxtCurrentTool.setText(R.string.lib);

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST_IMG);
                break;

            case EMOJI:
                mEmojiBSFragment.show(getActivity().getSupportFragmentManager(), mEmojiBSFragment.getTag());
                break;
            case STICKER:
                mStickerBSFragment.show(getActivity().getSupportFragmentManager(), mStickerBSFragment.getTag());
                break;
        }
    }


    void showFilter(boolean isVisible) {
        if (true) return;
        mIsFilterVisible = isVisible;
        mConstraintSet.clone(mRootView);

        if (isVisible) {
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                    ConstraintSet.PARENT_ID, ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.END,
                    ConstraintSet.PARENT_ID, ConstraintSet.END);
        } else {
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                    ConstraintSet.PARENT_ID, ConstraintSet.END);
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.END);
        }

        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.setDuration(350);
        changeBounds.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        TransitionManager.beginDelayedTransition(mRootView, changeBounds);

        mConstraintSet.applyTo(mRootView);
    }

//    @Override
//    public void onBackPressed() {
//        if (mIsFilterVisible) {
//            showFilter(false);
//            mTxtCurrentTool.setText(R.string.app_name);
//        } else if (!mPhotoEditor.isCacheEmpty()) {
//            showSaveDialog();
//        } else {
//            super.onBackPressed();
//        }
//    }


    @Override
    public void onFilterSelected(PhotoFilter photoFilter) {
        mPhotoEditor.setFilterEffect(photoFilter);
    }

    CustomProgressDialog customProgressDialog;

    public void uploadImg(byte[] uris) {


        String requestId = MediaManager.get().upload(uris).callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {
                Log.d(TAG, "onProgress: ");
            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                Log.d(TAG, "onSuccess: " + resultData.toString());
                customProgressDialog.dismiss();

                presenter.onOder(product.getId(), "1", resultData.get("secure_url").toString(),null);

            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        })
                .dispatch();

    }

    public void uploadImg(byte[] uris, byte[] svg) {

        String TAG = "UPLOADIMG";
        String requestId = MediaManager.get().upload(uris).callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {
                Log.d(TAG, "onProgress: ");
            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                Log.d(TAG, "onSuccess: IMG" + resultData.toString());
                String requestId2 = MediaManager.get().upload(svg).callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {
                        Log.d(TAG, "onProgress: ");
                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData2) {
                        Log.d(TAG, "onSuccess:SVG " + resultData2.toString());
                        customProgressDialog.dismiss();
                        presenter.onOder(product.getId(), "1", resultData.get("secure_url").toString(), resultData2.get("secure_url").toString());

                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        Log.d(TAG, "onError: " + error.toString());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                        .dispatch();
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        })
                .dispatch();


    }

    @Override
    public void nextAct() {
        Log.d(TAG, "nextAct: ");
        getActivity().startActivity(OrderActivity.intent(getContext()));
        getActivity().finish();
    }

    interface OnUploadImage {
        void ok();

        void fail();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_STORAGE:
                isPermissionGranted(grantResults[0] == PackageManager.PERMISSION_GRANTED, permissions[0]);
                break;
        }
    }

    public void isPermissionGranted(boolean isGranted, String permission) {
        if (isGranted) {
            saveImage();
        }
    }
}
