package com.nam.quy.design_shop.ui.design;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;

public interface DesignContract {
    interface View extends BaseView<Presenter> {
        void nextAct();
    }

    interface Presenter extends BasePresenter {
        void onOder(String idpro,String num,String url,String svg);
    }
}