package com.nam.quy.design_shop.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.nam.quy.design_shop.data.source.remote.response.User;
import com.google.gson.Gson;

public class Pref {
    private static final String FILE_NAME = "com.design.shop.android";
    private static final String FILE_NAME_NOT_RELATE_TO_USER = "com.design.shop.android.android.not.relate.to.user";

    private final static String KEY_AUTH = "key_auth";

    private Pref() {
    }

    public static SharedPreferences getDefaultPref(Context context) {
        return context.getSharedPreferences(FILE_NAME_NOT_RELATE_TO_USER, Context.MODE_PRIVATE);
    }

    public static SharedPreferences getUserPref(Context context) {
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void saveAuth(Context context, User user) {
        final Gson gson = new Gson();
        String json = gson.toJson(user);

        getUserPref(context)
                .edit()
                .putString(KEY_AUTH, json)
                .apply();
    }

    public static User getAuth(Context context) {
        final Gson gson = new Gson();
        String json = getUserPref(context).getString(KEY_AUTH, null);
        if (!TextUtils.isEmpty(json)) {
            return gson.fromJson(json, User.class);
        }
        return null;
    }

    public static void reset(Context context) {
        getUserPref(context).edit().clear().apply();
    }
}
