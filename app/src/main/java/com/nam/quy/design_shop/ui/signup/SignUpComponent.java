package com.nam.quy.design_shop.ui.signup;


import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = SignUpPresenterModule.class)
public interface SignUpComponent {
    void inject(SignUpFragment fragment);
}
