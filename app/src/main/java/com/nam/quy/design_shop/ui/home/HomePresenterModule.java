package com.nam.quy.design_shop.ui.home;


import dagger.Module;
import dagger.Provides;

@Module
public class HomePresenterModule {
    private final HomeContract.View view;

    public HomePresenterModule(HomeContract.View view) {
        this.view = view;
    }

    @Provides
    HomeContract.View provideTasksContractView() {
        return view;
    }
}

