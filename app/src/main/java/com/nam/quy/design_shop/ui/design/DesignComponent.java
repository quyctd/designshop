package com.nam.quy.design_shop.ui.design;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = DesignPresenterModule.class)
public interface DesignComponent {
    void inject(DesignFragment fragment);
}