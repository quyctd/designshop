package com.nam.quy.design_shop.ui.profile;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfilePresenterModule {
    private final ProfileContract.View view;

    public ProfilePresenterModule(ProfileContract.View view) {
        this.view = view;
    }

    @Provides
    ProfileContract.View provideTasksContractView() {
        return view;
    }
}