package com.nam.quy.design_shop.ui.buy.ConfirmOder;

import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.scope.FragmentScoped;

import dagger.Component;

@FragmentScoped
@Component(dependencies = DSRepositoryCompoment.class, modules = ConfirmPresenterModule.class)
public interface ConfirmComponent {
    void inject(ConfirmFragment fragment);
}
