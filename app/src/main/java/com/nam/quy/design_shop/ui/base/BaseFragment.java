package com.nam.quy.design_shop.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.nam.quy.design_shop.util.UiUtil;
import com.trello.rxlifecycle2.components.support.RxFragment;

/**
 * Created by quydc
 */
public class BaseFragment extends RxFragment {
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = getView();
        if (view != null) {
            UiUtil.hideKeyboardWhenTouchOutside(getActivity(), view.getRootView());
        }
    }
}
