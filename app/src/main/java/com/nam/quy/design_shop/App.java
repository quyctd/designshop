package com.nam.quy.design_shop;

import android.app.Application;
import android.content.Context;


import com.cloudinary.android.MediaManager;
import com.nam.quy.design_shop.data.source.DSRepositoryCompoment;
import com.nam.quy.design_shop.data.source.DaggerDSRepositoryCompoment;
import com.nam.quy.design_shop.net.NetModule;
import com.nam.quy.design_shop.util.MessageHandler;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.plugins.RxJavaPlugins;

public class App extends Application {
    private DSRepositoryCompoment dsRepositoryComponent;
    private MessageHandler messageHandler;
    private Context context;

    private static App instant;

    public static App getInstant() {

        return instant;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instant = this;
        context = getApplicationContext();

        final ApplicationModule appModule = new ApplicationModule((getApplicationContext()));
        dsRepositoryComponent = DaggerDSRepositoryCompoment.builder()
                .applicationModule(appModule)
                .netModule(new NetModule(Config.DS_URL))
                .build();
        RxJavaPlugins.setErrorHandler(e -> {
        });
        messageHandler = new MessageHandler(this,
                dsRepositoryComponent.provideRxBus());

        Map<String, String> config = new HashMap<String, String>();
        config.put("cloud_name", "flask-image");
        config.put("api_key", "133444264233997");
        config.put("api_secret", "SrlSO-4T4W2lQx72PEYGHSEnOwU");
        MediaManager.init(this, config);


    }

    public Context getContext() {
        return context;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public DSRepositoryCompoment getDsRepositoryComponent() {
        return dsRepositoryComponent;
    }
}
