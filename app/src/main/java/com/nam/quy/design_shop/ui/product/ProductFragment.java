package com.nam.quy.design_shop.ui.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nam.quy.design_shop.App;
import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseFragment;
import com.nam.quy.design_shop.ui.design.DesignActivity;
import com.nam.quy.design_shop.ui.design.editer.view.EditImageActivity;
import com.nam.quy.design_shop.ui.order.OrderActivity;
import com.nam.quy.design_shop.util.Formatter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProductFragment extends BaseFragment implements ProductContract.View {
    @Inject
    ProductPresenter presenter;

    Unbinder unbinder;
    Product product;

    public static ProductFragment newInstance(Product product) {
        ProductFragment fragment = new ProductFragment();
        fragment.product = product;
        return fragment;
    }

    @BindView(R.id.img_product)
    ImageView imgProduct;
    @BindView(R.id.tv_product)
    TextView tvProduct;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_desciprtion)
    TextView tvDesciption;

    @OnClick({R.id.img_back, R.id.tv_design,R.id.img_order})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                getActivity().onBackPressed();
                break;
            }
            case R.id.tv_design:
               // presenter.addOrder(product.getId(),"1");
                Intent i =DesignActivity.intent(getContext());
                i.putExtra("DATA",product);
                getActivity().startActivity(i);
                break;
            case R.id.img_order: {
                startActivity(OrderActivity.intent(getContext()));
                break;
            }
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerProductComponent.builder()
                .productPresenterModule(new ProductPresenterModule(this))
                .dSRepositoryCompoment(((App) getContext().getApplicationContext()).getDsRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.subscribe();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_frg, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Glide.with(getContext())
                .load(product.getImage())
                .into(imgProduct);
        tvProduct.setText(product.getName());
        tvPrice.setText(Formatter.costToVnd(product.getPrice()));
        tvDesciption.setText(product.getDescription());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.unsubscribe();
    }

}
