package com.nam.quy.design_shop.ui.order;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;
import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.data.source.remote.response.Product;

import java.util.List;

public interface OrderContract {
    interface View extends BaseView<Presenter> {
        void onLoadedOrders(List<Order> orders);
        void deleteOrderSuccess();
        void deleteOrderFail();
    }
    interface Presenter extends BasePresenter {
        void deleteOrder(String id);
        void buyAll();
    }
}
