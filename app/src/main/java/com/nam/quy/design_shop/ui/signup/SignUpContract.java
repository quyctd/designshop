package com.nam.quy.design_shop.ui.signup;

import com.nam.quy.design_shop.BasePresenter;
import com.nam.quy.design_shop.BaseView;

public interface SignUpContract {
    interface View extends BaseView<Presenter> {
        void onSignUpSuccess();

        void onSignUpError();

        void onLoginError();

        void onLoginSuccess();
    }

    interface Presenter extends BasePresenter {
        void signUp(String name, String email, String password);
        void login(String user,String password);
    }
}
