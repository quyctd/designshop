package com.nam.quy.design_shop.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("accountId")
    @Expose
    public String accountId;
    @SerializedName("name")
    @Expose
    public String name;
}
