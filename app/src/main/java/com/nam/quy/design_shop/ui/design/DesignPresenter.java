package com.nam.quy.design_shop.ui.design;

import android.util.Log;

import com.nam.quy.design_shop.data.source.DSRepository;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class DesignPresenter implements DesignContract.Presenter {

    private final String TAG = "HomePresenter";
    private final DesignContract.View view;
    private final UserManager userManager;
    private final DSRepository dsRepository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    @Inject
    DesignPresenter(DesignContract.View view, UserManager userManager, DSRepository dsRepository, BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.userManager = userManager;
        this.dsRepository = dsRepository;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void onOder(String idpro, String num,String url,String svg) {
        Disposable getOrdersDisposable = dsRepository.addOrder(userManager.getUser().id, idpro, "1",url,svg)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(jsonObjectResponse -> {
                    Timber.tag(TAG).i("getOrders: %s", jsonObjectResponse.data);

                   if (jsonObjectResponse.isSuccess()){
                       //view.nextAct();
                   }
                    view.nextAct();

                }, throwable -> {
                    Log.d(TAG, "onOder: "+throwable.getMessage());
                    Timber.tag(TAG).e(throwable);
                });
        compositeDisposable.add(getOrdersDisposable);
    }
}
