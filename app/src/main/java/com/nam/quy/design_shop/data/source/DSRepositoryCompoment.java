package com.nam.quy.design_shop.data.source;


import com.nam.quy.design_shop.ApplicationModule;
import com.nam.quy.design_shop.manager.UserManager;
import com.nam.quy.design_shop.net.NetModule;
import com.nam.quy.design_shop.util.RxBus;
import com.nam.quy.design_shop.util.schedulers.BaseSchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Provides;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface DSRepositoryCompoment {
    RxBus provideRxBus();

    DSRepository getDSRepository();

    UserManager provideUserManager();

    BaseSchedulerProvider provideScheduler();
}
