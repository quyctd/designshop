package com.nam.quy.design_shop.ui.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nam.quy.design_shop.R;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.ui.base.BaseActivity;
import com.nam.quy.design_shop.util.ActivityUtils;

import butterknife.ButterKnife;

public class ProductActivity extends BaseActivity{
        public static Intent intent(Context context,Product product) {
            Intent intent = new Intent(context, ProductActivity.class);
            intent.putExtra("product",product);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            return intent;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.box_act);
            ButterKnife.bind(this);
            ProductFragment productFragment =
                    (ProductFragment) getSupportFragmentManager().findFragmentById(R.id.container);
            if (productFragment == null) {
                productFragment = ProductFragment.newInstance((Product) getIntent().getSerializableExtra("product"));
                ActivityUtils.addFragmentToActivity(
                        getSupportFragmentManager(), productFragment, R.id.container);
            }
        }
}
