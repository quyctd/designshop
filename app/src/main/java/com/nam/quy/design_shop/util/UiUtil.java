package com.nam.quy.design_shop.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Created by nam
 */
public class UiUtil {
    private UiUtil() {
    }

    /**
     * tint color for the list of given drawables. It will auto skip all null drawable.
     *
     * @param tint      the tint color
     * @param drawables the given list of drawable
     */
    public static void tintDrawables(int tint, Drawable... drawables) {
        if (drawables == null || drawables.length == 0) {
            return;
        }
        for (Drawable drawable : drawables) {
            if (drawable == null) continue;
            DrawableCompat.setTint(drawable, tint);
        }
    }

    /**
     * make textview handleable click on drawable
     *
     * @param textView the text view
     * @param position the position of drawables.
     * @param listener handle action when user click.
     */
    public static void setOnDrawableClick(TextView textView, Position position, View.OnTouchListener listener) {
        textView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP && listener != null) {
                int drawableWidth = textView.getCompoundDrawables()[position.getValue()]
                        .getBounds().width();
                if (event.getRawX() >= (textView.getRight() - drawableWidth)) {
                    return listener.onTouch(v, event);
                }
            }
            return false;
        });
    }

    public static void showRefreshing(final SwipeRefreshLayout refreshLayout) {
        if (refreshLayout == null) return;
        refreshLayout.post(() -> refreshLayout.setRefreshing(true));
    }

    public static void hideRefreshing(final SwipeRefreshLayout refreshLayout) {
        if (refreshLayout == null) return;
        refreshLayout.post(() -> refreshLayout.setRefreshing(false));
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

//    @SuppressWarnings("unchecked")
//    public static Observable<Bitmap> getBitmapFromBase64(Context context, String base64Data, Transformation<Bitmap> transform) {
//        if (base64Data == null) {
//            return null;
////            return Observable.fromCallable(() -> {
////                throw new RuntimeException("error format data");
////            });
//        }
//
//        return Observable.just(base64Data)
//                .filter(imageBase64Data -> !TextUtils.isEmpty(imageBase64Data))
//                .map(imageBase64Data -> Base64.decode(imageBase64Data, Base64.DEFAULT))
//                .map(bytes -> {
//                    BitmapTypeRequest<byte[]> request = Glide.with(context)
//                            .load(bytes)
//                            .asBitmap();
//                    if (transform != null) {
//                        request.transform(transform);
//                    }
//                    return request.into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
//                            .get();
//                });
//    }

    public static void hideKeyboardWhenTouchOutside(Activity activity, View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard(activity);
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboardWhenTouchOutside(activity, innerView);
            }
        }
    }

    public enum Position {
        LEFT(0),
        TOP(1),
        RIGHT(2),
        BOTTOM(3);

        private final int value;

        Position(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
