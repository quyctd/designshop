package com.nam.quy.design_shop.data.source;

import com.nam.quy.design_shop.data.source.remote.response.Order;
import com.nam.quy.design_shop.data.source.remote.response.Product;
import com.nam.quy.design_shop.data.source.remote.response.Response;
import com.nam.quy.design_shop.data.source.remote.response.User;
import com.google.gson.JsonObject;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class DSRepository implements DSDataSource {

    private final DSDataSource dsRemoteDataSource;

    @Inject
    public DSRepository(DSDataSource dsRemoteDataSource) {
        this.dsRemoteDataSource = dsRemoteDataSource;
    }

    @Override
    public Observable<Response<User>> login(String accountId, String password) {
        return dsRemoteDataSource.login(accountId,password);
    }

    @Override
    public Observable<Response<JsonObject>> signUp(String name, String email, String password,String confirm) {
        return dsRemoteDataSource.signUp(name,email,password,confirm);
    }

    @Override
    public Observable<Response<List<Product>>> listProducts() {
        return dsRemoteDataSource.listProducts();
    }

    @Override
    public Observable<Response<JsonObject>> addOrder(String userId, String productId, String number,String url,String svg) {
        return dsRemoteDataSource.addOrder(userId,productId,number,url,svg);
    }

    @Override
    public Observable<Response<List<Order>>> listOrder(String userId) {
        return dsRemoteDataSource.listOrder(userId);
    }

    @Override
    public Observable<Response<JsonObject>> deleteOrder(String orderId) {
        return dsRemoteDataSource.deleteOrder(orderId);
    }

    @Override
    public Observable<Response<JsonObject>> buyAll(String userId) {
        return dsRemoteDataSource.buyAll(userId);
    }

    @Override
    public Observable<Response<JsonObject>> changeOrderStatus(String orderId) {
        return dsRemoteDataSource.changeOrderStatus(orderId);
    }

    @Override
    public Observable<Response<JsonObject>> addReceiver(String userId, String name, String phone, String address) {
        return dsRemoteDataSource.addReceiver(userId,name,phone,address);
    }

    @Override
    public Observable<Response<List<Order>>> getMyOrder(String id) {
        return dsRemoteDataSource.getMyOrder(id);
    }

    @Override
    public Observable<Response<List<Order>>> getAllOrder(String id) {
        return dsRemoteDataSource.getAllOrder(id);
    }

}
