package com.nam.quy.design_shop.ui.design.editer.view.tools;

/**
 * @author <a href="https://github.com/burhanrashid52">Burhanuddin Rashid</a>
 * @version 0.1.2
 * @since 5/23/2018
 */
public enum ToolType {
    BRUSH,
    TEXT,
    ERASER,
    IMAGE,
    EMOJI,
    CAMERA,
    STICKER;

}
